﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class MyStack3<T>
    {
        T[] stack = new T[1000];
        int n = 0;

        public void push(T name)
        {
            if (n%1000 == 0)
            {
                T[] bufer = new T[n+1000];
                for (int i = 0; n > i; i++)
                    bufer[i] = stack[i];
                n++;
                stack = null;
                stack = new T[n+1000];
                for (int i = 0; n - 1 > i; i++)
                    stack[i] = bufer[i];
                stack[n - 1] = name;
            }
            else
            {
                n++;
                stack[n - 1] = name;
            }
        }
        public void pop()
        {
            if (n > 0)
            {
                n--;
                stack[n] = default(T);
            }
        }
        public bool empty()
        {
            if (n == 0)
                return true;
            else
                return false;

        }
        public int size()
        {
            return n;
        }
        public T top()
        {
            if (n > 0)
                return stack[n - 1];
            else
                return default(T);
        }

    }
}
