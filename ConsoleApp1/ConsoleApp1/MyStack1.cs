﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class MyStack1<T>
    {
        T[] stack = new T[1000000];
        int n = 0;

        public void push(T name)
        {
            n++;
            stack[n - 1] = name;
        }
        public void pop()
        {
            if (n > 0)
            {
                n--;
                stack[n] = default(T);
            }
        }
        public bool empty()
        {
            if (n == 0)
                return true;
            else
                return false;

        }
        public int size()
        {
            return n;
        }
        public T top()
        {
            if (n > 0)
                return stack[n - 1];
            else
                return default(T);
        }

    }
}
