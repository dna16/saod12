﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class MyStack4<T>
    {
        List<T> stack = new List<T>(0);
        int n = 0;

        public void push(T name)
        {
            stack.Add(name);
            n++;
        }
        public void pop()
        {
            if (n > 0)
            {
                stack.RemoveAt(n - 1);
                n--;
            }
        }
        public bool empty()
        {
            if (n == 0)
                return true;
            else
                return false;

        }
        public int size()
        {
            return n;
        }
        public T top()
        {
            if (n > 0)
                return stack[n - 1];
            else
                return default(T);
        }

    }
}
