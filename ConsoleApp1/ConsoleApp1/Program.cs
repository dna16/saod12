﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Diagnostics.Stopwatch watch;
            long elapsedMs;
            int N = 100000;

            var stack = new System.Collections.Generic.Stack<int>();
            watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i != N; i++)
            {
                stack.Push(i);
            }
            for (int i = 0; i != N; i++)
            {
                stack.Pop();
            }
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("Stock = " + elapsedMs);
            watch = null;


            MyStack1<int> s1 = new MyStack1<int>();
            watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i != N; i++)
            {
                s1.push(i);
            }
            for (int i = 0; i != N; i++)
            {
                s1.pop();
            }
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("Fix array(1000000) = " + elapsedMs);
            watch = null;


            MyStack2<int> s2 = new MyStack2<int>();
            watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i != N; i++)
            {
                s2.push(i);
            }
            for (int i = 0; i != N; i++)
            {
                s2.pop();
            }
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("Array.Resize(1000) = " + elapsedMs);
            watch = null;


            MyStack3<int> s3 = new MyStack3<int>();
            watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i != N; i++)
            {
                s3.push(i);
            }
            for (int i = 0; i != N; i++)
            {
                s3.pop();
            }
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("New array(1000) = " + elapsedMs);
            watch = null;


            MyStack4<int> s4 = new MyStack4<int>();
            watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i != N; i++)
            {
                s4.push(i);
            }
            for (int i = 0; i != N; i++)
            {
                s4.pop();
            }
            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("List = " + elapsedMs);
            watch = null;

            Console.ReadLine();
        }
    }
}
